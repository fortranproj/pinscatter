      complex*16 function apip_roex(s,costh,lbd1,lbd2)
      implicit none
      real*8 s,costh,pmod,costh_2,xLbd,k1(0:3),Ek,p1(0:3),sinth,
     &p1pp2(0:3),k1pk2p1pp2,ilo_sk_4d,k2(0:3),p2(0:3),Ep,t,k1pk2(0:3),
     &ff
      real*8 pm,pim,rom,omegm
      integer lbd1,lbd2,i,j
      complex*16 u1(1:4),u2(1:4),u2bar(1:4),ck1pk2(0:3),
     &ck1pk2sl(1:4,1:4),ck1pk2p1pp2,ck1pk2p1pp2mat(1:4,1:4),
     &propag
!      ,u1bar(1:4)
      real*8 pi,e,gpipiro,GrhoV,GrhoT,GomV,GomT,groompi,gropigam,
     &gompigam,CONV
      complex*16 d0(1:4,1:4),d1(1:4,1:4),d2(1:4,1:4),d3(1:4,1:4),
     &d5(1:4,1:4),unit(1:4,1:4)
      common/masses/pm,pim,rom,omegm
      common/stale/pi,e,gpipiro,GrhoV,GrhoT,GomV,GomT,groompi,gropigam,
     &gompigam,CONV
      common/gammas/d0,d1,d2,d3,d5,unit
      
      pmod=dsqrt(xLbd(s,pm**2,pim**2)/(4.d0*s))
      call bispinor(u1,pmod,0.d0,0.d0,lbd1)
      costh_2=dsqrt((1.d0-costh)/2.d0)
      call bispinor(u2,pmod,costh_2,0.d0,lbd2)
      call dirconj(u2,u2bar)
!       call dirconj(u1,u1bar)
      
      Ek=(s+pim**2-pm**2)/(2.d0*dsqrt(s))
      
      k1(0)=Ek
      k1(1)=0.d0
      k1(2)=0.d0
      k1(3)=pmod
      
      Ep=(s+pm**2-pim**2)/(2.d0*dsqrt(s))
      
      p1(0)=Ep
      do i=1,3
        p1(i)=-k1(i)
      enddo
      
      sinth=dsqrt(1.d0-costh**2)
      
      p2(0)=Ep
      p2(1)=-pmod*sinth
      p2(2)=0.d0
      p2(3)=-pmod*costh
      
      k2(0)=Ek
      do i=1,3
      k2(i)=-p2(i)
      enddo
      
      
      call v4plusv4(p1,p2,p1pp2)
      call v4plusv4(k1,k2,k1pk2)
      
      k1pk2p1pp2=ilo_sk_4d(k1pk2,p1pp2)
      ck1pk2p1pp2=dcmplx(k1pk2p1pp2)
      
      
      call complexify(k1pk2,ck1pk2)
      call v4xgam(ck1pk2,ck1pk2sl)
      
      call scalarxgam(ck1pk2p1pp2,unit,ck1pk2p1pp2mat) 
      
      apip_roex=dcmplx(0.d0)
      t=2.d0*pmod**2*(costh-1.d0)
      do i=1,4
      do j=1,4
      apip_roex=apip_roex+propag(s,t,rom)*ff(t)*gpipiro*u2bar(i)*(
     &(GrhoV+GrhoT)*ck1pk2sl(i,j)
     &-GrhoT/(2.d0*pm)*ck1pk2p1pp2mat(i,j)
     &)*u1(j)
      enddo
      enddo
      end
      
      complex*16 function propag(s,t,mv)
      implicit none
      complex*16 regge
      integer iprop
      real*8 mv,s,t
      common/propagator/iprop
      if(iprop.eq.0) then
      propag=1.d0/(t-mv**2)
      else
      propag=Regge(mv,s,t)
      endif
      end
      
      real*8 function ff(t)
      implicit none
      real*8 :: t,lbd=1.4d0
      ff=lbd**2/(lbd**2-t)
      end
