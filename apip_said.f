      real*8 function totalcspiplp(s,t)
      implicit none
      real*8 pm,pim,rom,omegm
      real*8 s,t,pmod,xLbd
      complex*16 Tpiplp
      common/masses/pm,pim,rom,omegm
      pmod=dsqrt(xLbd(s,pm**2,pim**2)/(4.d0*s))
      totalcspiplp=dimag(Tpiplp(s,0.d0,1,1)+Tpiplp(s,0.d0,1,-1))/
     &(2.d0*pmod*dsqrt(s))
      end
      
      complex*16 function Tpiplp(s,t,lbd1,lbd2)
      implicit none
      real*8 s,t,costh,sinth,pmod,costh_2,xLbd,k1(0:3),Ek,
     &k2(0:3),p1(0:3),p2(0:3),Ep,k1pk2(0:3)
      integer lbd1,lbd2,i,j
      real*8 pm,pim,rom,omegm
      complex*16 u1(1:4),u2(1:4),u2bar(1:4),AMat(1:4,1:4),
     &A,B,ck1pk2(0:3),ck1pk2sl(1:4,1:4),A32,B32
      complex*16 d0(1:4,1:4),d1(1:4,1:4),d2(1:4,1:4),d3(1:4,1:4),
     &d5(1:4,1:4),unit(1:4,1:4)
      common/masses/pm,pim,rom,omegm
      common/gammas/d0,d1,d2,d3,d5,unit
      
      pmod=dsqrt(xLbd(s,pm**2,pim**2)/(4.d0*s))
      
      Ek=(s+pim**2-pm**2)/(2.d0*dsqrt(s))
      
      k1(0)=Ek
      k1(1)=0.d0
      k1(2)=0.d0
      k1(3)=pmod
      
      Ep=(s+pm**2-pim**2)/(2.d0*dsqrt(s))
      
      p1(0)=Ep
      do i=1,3
        p1(i)=-k1(i)
      enddo
      
      costh=1.d0+t/(2.d0*pmod**2)
      sinth=dsqrt(1.d0-costh**2)
      
      p2(0)=Ep
      p2(1)=-pmod*sinth
      p2(2)=0.d0
      p2(3)=-pmod*costh
      
      k2(0)=Ek
      do i=1,3
      k2(i)=-p2(i)
      enddo
      
      call bispinor(u1,pmod,0.d0,0.d0,lbd1)
      costh_2=dsqrt((1.d0-costh)/2.d0) !! the "-" sign is because p2 direction is pi-theta !!
      call bispinor(u2,pmod,costh_2,0.d0,lbd2)
      call dirconj(u2,u2bar)
      
      call scalarxgam(A32(s,t),unit,AMat)
      call v4plusv4(k1,k2,k1pk2)
      
      call complexify(k1pk2,ck1pk2)
      call v4xgam(ck1pk2,ck1pk2sl)
      
      Tpiplp=dcmplx(0.d0)
      B=B32(s,t)
      
      do i=1,4
      do j=1,4
      Tpiplp=Tpiplp+u2bar(i)*(
     &AMat(i,j)
     &+0.5d0*ck1pk2sl(i,j)*B
     &)*u1(j)
      enddo
      enddo
      
      end
      
      complex*16 function Tminus(s,costh,lbd1,lbd2)
      implicit none
      real*8 s,costh,pmod,costh_2,xLbd,k1(0:3),Ek,p1(0:3),sinth,
     &k2(0:3),p2(0:3),Ep,t,k1pk2(0:3)
      integer lbd1,lbd2,i,j
      real*8 pm,pim,rom,omegm
      complex*16 u1(1:4),u2(1:4),u2bar(1:4),AminusMat(1:4,1:4),
     &Aminus,Bminus,Bmin,ck1pk2(0:3),ck1pk2sl(1:4,1:4)
      complex*16 d0(1:4,1:4),d1(1:4,1:4),d2(1:4,1:4),d3(1:4,1:4),
     &d5(1:4,1:4),unit(1:4,1:4)
      common/masses/pm,pim,rom,omegm
      common/gammas/d0,d1,d2,d3,d5,unit
      
      pmod=dsqrt(xLbd(s,pm**2,pim**2)/(4.d0*s))
      
      Ek=(s+pim**2-pm**2)/(2.d0*dsqrt(s))
      
      k1(0)=Ek
      k1(1)=0.d0
      k1(2)=0.d0
      k1(3)=pmod
      
      Ep=(s+pm**2-pim**2)/(2.d0*dsqrt(s))
      
      p1(0)=Ep
      do i=1,3
        p1(i)=-k1(i)
      enddo
      
      sinth=dsqrt(1.d0-costh**2)
      
      p2(0)=Ep
      p2(1)=-pmod*sinth
      p2(2)=0.d0
      p2(3)=-pmod*costh
      
      k2(0)=Ek
      do i=1,3
      k2(i)=-p2(i)
      enddo
      
      call bispinor(u1,pmod,0.d0,0.d0,lbd1)
      costh_2=dsqrt((1.d0-costh)/2.d0)
      call bispinor(u2,pmod,costh_2,0.d0,lbd2)
      call dirconj(u2,u2bar)
      
      t=2.d0*pmod**2*(costh-1.d0)
      
      call scalarxgam(Aminus(s,t),unit,AminusMat)
      call v4plusv4(k1,k2,k1pk2)
      
      call complexify(k1pk2,ck1pk2)
      call v4xgam(ck1pk2,ck1pk2sl)
      
      Tminus=dcmplx(0.d0)
      Bmin=Bminus(s,t)
      
      do i=1,4
      do j=1,4
      Tminus=Tminus+u2bar(i)*(
     &AminusMat(i,j)
     &+0.5d0*ck1pk2sl(i,j)*Bmin
     &)*u1(j)
      enddo
      enddo
      if(.not.(Tminus.eq.Tminus)) then
      print*,u2
      endif
      
      end
      
      complex*16 function Aminus(s,t)
      implicit none
      complex*16 A12,A32
      real*8 s,t
      Aminus=1.d0/3.d0*(A12(s,t)-A32(s,t))
      end
      
      complex*16 function Bminus(s,t)
      implicit none
      complex*16 B12,B32
      real*8 s,t
      Bminus=1.d0/3.d0*(B12(s,t)-B32(s,t))
      end
      
      complex*16 function A12(s,t)
      implicit none
      real*8 s,t,w,Eprotcm
      complex*16 f1_12,f2_12
      real*8 pi,e,gpipiro,GrhoV,GrhoT,GomV,GomT,groompi,gropigam,
     &gompigam,CONV
      real*8 pm,pim,rom,omegm
      common/stale/pi,e,gpipiro,GrhoV,GrhoT,GomV,GomT,groompi,gropigam,
     &gompigam,CONV
      common/masses/pm,pim,rom,omegm
      w=dsqrt(s)
      Eprotcm=(W**2+PM**2-PIM**2)/(2.0d0*W)
      A12=4.0d0*PI*((W+PM)/(Eprotcm+PM)*f1_12(w,t,pim**2)-
     &(W-PM)/(Eprotcm-PM)*f2_12(w,t,pim**2))
      end
       
      complex*16 function B12(s,t)
      implicit none
      real*8 s,t,w,Eprotcm
      complex*16 f1_12,f2_12
      real*8 pi,e,gpipiro,GrhoV,GrhoT,GomV,GomT,groompi,gropigam,
     &gompigam,CONV
      real*8 pm,pim,rom,omegm
      common/stale/pi,e,gpipiro,GrhoV,GrhoT,GomV,GomT,groompi,gropigam,
     &gompigam,CONV
      common/masses/pm,pim,rom,omegm
      w=dsqrt(s)
      Eprotcm=(W**2+PM**2-PIM**2)/(2.0d0*W)
      B12=4.0d0*PI*(f1_12(w,t,pim**2)/(Eprotcm+PM)+f2_12(w,t,pim**2)/
     &(Eprotcm-PM))
      end
      
      complex*16 function A32(s,t)
      implicit none
      real*8 s,t,w,Eprotcm
      complex*16 f1_32,f2_32
      real*8 pi,e,gpipiro,GrhoV,GrhoT,GomV,GomT,groompi,gropigam,
     &gompigam,CONV
      real*8 pm,pim,rom,omegm
      common/stale/pi,e,gpipiro,GrhoV,GrhoT,GomV,GomT,groompi,gropigam,
     &gompigam,CONV
      common/masses/pm,pim,rom,omegm
      w=dsqrt(s)
      Eprotcm=(W**2+PM**2-PIM**2)/(2.0d0*W)
      A32=4.0d0*PI*((W+PM)/(Eprotcm+PM)*f1_32(w,t,pim**2)-
     &(W-PM)/(Eprotcm-PM)*f2_32(w,t,pim**2))
      end
       
      complex*16 function B32(s,t)
      implicit none
      real*8 s,t,w,Eprotcm
      complex*16 f1_32,f2_32
      real*8 pi,e,gpipiro,GrhoV,GrhoT,GomV,GomT,groompi,gropigam,
     &gompigam,CONV
      real*8 pm,pim,rom,omegm
      common/stale/pi,e,gpipiro,GrhoV,GrhoT,GomV,GomT,groompi,gropigam,
     &gompigam,CONV
      common/masses/pm,pim,rom,omegm
      w=dsqrt(s)
      Eprotcm=(W**2+PM**2-PIM**2)/(2.0d0*W)
      B32=4.0d0*PI*(f1_32(w,t,pim**2)/(Eprotcm+PM)+f2_32(w,t,pim**2)/
     &(Eprotcm-PM))
      end

! funkcje f1 i f2 wchodzące do amplitudy rozpraszania pi-p
      complex*16 function f1_12(w,t,ti)
      implicit none
      integer NW
      real*8 LegendrePrim,xm,pm2,p1,p2,xLbd,costhcm,w,t,ti,SPL3
      real*8 pm,pim,rom,omegm
      real*8 pi,e,gpipiro,GrhoV,GrhoT,GomV,GomT,groompi,gropigam,
     &gompigam,CONV
      real*8 ReS11,ReP13,ReD15,ReF17,ReG19,ReD13,ReF15,ReG17,ReH19,
     &ReP11,UrS11,UrP13,UrD15,UrF17,UrG19,UrD13,UrF15,UrG17,UrH19,
     &UrP11,ReS11DP,ReP13DP,ReD15DP,ReF17DP,ReG19DP,ReD13DP,ReF15DP,
     &ReG17DP,ReH19DP,ReP11DP,UrS11DP,UrP13DP,UrD15DP,UrF17DP,UrG19DP,
     &UrD13DP,UrF15DP,UrG17DP,UrH19DP,UrP11DP
      complex*16 CI,S11,P13,D15,F17,G19,D13,F15,G17,H19
      common/masses/pm,pim,rom,omegm
      common/stale/pi,e,gpipiro,GrhoV,GrhoT,GomV,GomT,groompi,gropigam,
     &gompigam,CONV
      parameter(NW=27)
      common/WEZLYPIP/xm(NW)
      common/ampl_pip12/
! I=1/2
     &ReS11(NW),UrS11(NW),ReS11DP(NW),UrS11DP(NW),
     &ReP13(NW),UrP13(NW),ReP13DP(NW),UrP13DP(NW),
     &ReD15(NW),UrD15(NW),ReD15DP(NW),UrD15DP(NW),
     &ReF17(NW),UrF17(NW),ReF17DP(NW),UrF17DP(NW),
     &ReG19(NW),UrG19(NW),ReG19DP(NW),UrG19DP(NW),
     &ReD13(NW),UrD13(NW),ReD13DP(NW),UrD13DP(NW),
     &ReF15(NW),UrF15(NW),ReF15DP(NW),UrF15DP(NW),
     &ReG17(NW),UrG17(NW),ReG17DP(NW),UrG17DP(NW),
     &ReH19(NW),UrH19(NW),ReH19DP(NW),UrH19DP(NW),
     &ReP11(NW),UrP11(NW),ReP11DP(NW),UrP11DP(NW)
      CI=DCMPLX(0.D0,1.D0)
      S11=SPL3(NW,xm,ReS11,ReS11DP,w)+CI*SPL3(NW,xm,UrS11,UrS11DP,w)
      P13=SPL3(NW,xm,ReP13,ReP13DP,w)+CI*SPL3(NW,xm,UrP13,UrP13DP,w)
      D15=SPL3(NW,xm,ReD15,ReD15DP,w)+CI*SPL3(NW,xm,UrD15,UrD15DP,w)
      F17=SPL3(NW,xm,ReF17,ReF17DP,w)+CI*SPL3(NW,xm,UrF17,UrF17DP,w)
      G19=SPL3(NW,xm,ReG19,ReG19DP,w)+CI*SPL3(NW,xm,UrG19,UrG19DP,w)
      D13=SPL3(NW,xm,ReD13,ReD13DP,w)+CI*SPL3(NW,xm,UrD13,UrD13DP,w)
      F15=SPL3(NW,xm,ReF15,ReF15DP,w)+CI*SPL3(NW,xm,UrF15,UrF15DP,w)
      G17=SPL3(NW,xm,ReG17,ReG17DP,w)+CI*SPL3(NW,xm,UrG17,UrG17DP,w)
      H19=SPL3(NW,xm,ReH19,ReH19DP,w)+CI*SPL3(NW,xm,UrH19,UrH19DP,w)
      
      pm2=pm**2
      
      p1=dsqrt(xLbd(w**2,pm**2,ti)/(4.d0*w**2))
      p2=dsqrt(xLbd(w**2,pm**2,pim**2)/(4.d0*w**2))
      
      costhcm=(t-2.d0*pm2+2.d0*dsqrt(p1**2+pm2)*dsqrt(p2**2+pm2))
     &/(2.d0*p1*p2)
     
!       print*,"f1_32 pm2=",pm2,"costh=",costhcm
      
      if(costhcm.lt.-1.d0 .or. costhcm.gt.1.d0) then
      print*,'costhcm=',costhcm,'p1=',p1,'p2=',p2,'w=',w,'ti=',ti
      endif
       f1_12=1.0d0/p2*(S11*LegendrePrim(1,costhcm)+
     & P13*LegendrePrim(2,costhcm)+D15*LegendrePrim(3,costhcm)+
     & F17*LegendrePrim(4,costhcm)+G19*LegendrePrim(5,costhcm)-
     & D13*LegendrePrim(1,costhcm)-F15*LegendrePrim(2,costhcm)-
     & G17*LegendrePrim(3,costhcm)-H19*LegendrePrim(4,costhcm)
     & )
       end

      complex*16 function f2_12(w,t,ti)
      implicit none
      real*8, intent(in):: w,t,ti
      integer NW
      real*8 LegendrePrim,xm,pm2,p1,p2,xLbd,costhcm,SPL3
      real*8 pm,pim,rom,omegm
      real*8 pi,e,gpipiro,GrhoV,GrhoT,GomV,GomT,groompi,gropigam,
     &gompigam,CONV
      real*8 ReS11,ReP13,ReD15,ReF17,ReG19,ReD13,ReF15,ReG17,ReH19,
     &ReP11,UrS11,UrP13,UrD15,UrF17,UrG19,UrD13,UrF15,UrG17,UrH19,
     &UrP11,ReS11Dp,ReP13DP,ReD15DP,ReF17DP,ReG19DP,ReD13DP,ReF15DP,
     &ReG17DP,ReH19DP,ReP11DP,UrS11DP,UrP13DP,UrD15DP,UrF17DP,UrG19DP,
     &UrD13DP,UrF15DP,UrG17DP,UrH19DP,UrP11DP
      complex*16 CI,S11,P13,D15,F17,G19,D13,F15,G17,H19,P11
      common/masses/pm,pim,rom,omegm
      common/stale/pi,e,gpipiro,GrhoV,GrhoT,GomV,GomT,groompi,gropigam,
     &gompigam,CONV
      parameter(NW=27)
      common/WEZLYPIP/xm(NW)
      common/ampl_pip12/
! I=1/2
     &ReS11(NW),UrS11(NW),ReS11DP(NW),UrS11DP(NW),
     &ReP13(NW),UrP13(NW),ReP13DP(NW),UrP13DP(NW),
     &ReD15(NW),UrD15(NW),ReD15DP(NW),UrD15DP(NW),
     &ReF17(NW),UrF17(NW),ReF17DP(NW),UrF17DP(NW),
     &ReG19(NW),UrG19(NW),ReG19DP(NW),UrG19DP(NW),
     &ReD13(NW),UrD13(NW),ReD13DP(NW),UrD13DP(NW),
     &ReF15(NW),UrF15(NW),ReF15DP(NW),UrF15DP(NW),
     &ReG17(NW),UrG17(NW),ReG17DP(NW),UrG17DP(NW),
     &ReH19(NW),UrH19(NW),ReH19DP(NW),UrH19DP(NW),
     &ReP11(NW),UrP11(NW),ReP11DP(NW),UrP11DP(NW)
      CI=DCMPLX(0.D0,1.D0)
      P11=SPL3(NW,xm,ReP11,ReP11DP,w)+CI*SPL3(NW,xm,UrP11,UrP11DP,w)
      P13=SPL3(NW,xm,ReP13,ReP13DP,w)+CI*SPL3(NW,xm,UrP13,UrP13DP,w)
      D13=SPL3(NW,xm,ReD13,ReD13DP,w)+CI*SPL3(NW,xm,UrD13,UrD13DP,w)
      D15=SPL3(NW,xm,ReD15,ReD15DP,w)+CI*SPL3(NW,xm,UrD15,UrD15DP,w)
      F15=SPL3(NW,xm,ReF15,ReF15DP,w)+CI*SPL3(NW,xm,UrF15,UrF15DP,w)
      F17=SPL3(NW,xm,ReF17,ReF17DP,w)+CI*SPL3(NW,xm,UrF17,UrF17DP,w)
      G17=SPL3(NW,xm,ReG17,ReG17DP,w)+CI*SPL3(NW,xm,UrG17,UrG17DP,w)
      G19=SPL3(NW,xm,ReG19,ReG19DP,w)+CI*SPL3(NW,xm,UrG19,UrG19DP,w)
      
      pm2=pm**2
      
      p1=dsqrt(xLbd(w**2,pm**2,ti)/(4.d0*w**2))
      p2=dsqrt(xLbd(w**2,pm**2,pim**2)/(4.d0*w**2))
      
      costhcm=(t-2.d0*pm2+2.d0*dsqrt(p1**2+pm2)*dsqrt(p2**2+pm2))
     &/(2.d0*p1*p2)
       
!        print*,"f2_12 pm2=",pm2,"costh=",costhcm,"pm**2=",pm**2
      
       f2_12=1.0d0/p2*((P11-P13)*LegendrePrim(1,costhcm)+
     & (D13-D15)*LegendrePrim(2,costhcm)+
     & (F15-F17)*LegendrePrim(3,costhcm)+
     & (G17-G19)*LegendrePrim(4,costhcm)
     & )
       end

      complex*16 function f1_32(w,t,ti)
      implicit none
      integer NW
      real*8 LegendrePrim,xm,pm2,p1,p2,xLbd,costhcm,w,t,ti,SPL3
      real*8 pm,pim,rom,omegm
      real*8 pi,e,gpipiro,GrhoV,GrhoT,GomV,GomT,groompi,gropigam,
     &gompigam,CONV
      real*8 ReS31,UrS31,ReS31DP,UrS31DP,
     &ReP33,UrP33,ReP33DP,UrP33DP,
     &ReD35,UrD35,ReD35DP,UrD35DP,
     &ReF37,UrF37,ReF37DP,UrF37DP,
     &ReG39,UrG39,ReG39DP,UrG39DP,
     &ReD33,UrD33,ReD33DP,UrD33DP,
     &ReF35,UrF35,ReF35DP,UrF35DP,
     &ReG37,UrG37,ReG37DP,UrG37DP,
     &ReH39,UrH39,ReH39DP,UrH39DP,
     &ReP31,UrP31,ReP31DP,UrP31DP
      complex*16 CI,S31,P33,D35,F37,G39,D33,F35,G37,H39
      common/masses/pm,pim,rom,omegm
      common/stale/pi,e,gpipiro,GrhoV,GrhoT,GomV,GomT,groompi,gropigam,
     &gompigam,CONV
      parameter(NW=27)
      common/WEZLYPIP/xm(NW)
      common/ampl_pip32/
! I=3/2
     &ReS31(NW),UrS31(NW),ReS31DP(NW),UrS31DP(NW),
     &ReP33(NW),UrP33(NW),ReP33DP(NW),UrP33DP(NW),
     &ReD35(NW),UrD35(NW),ReD35DP(NW),UrD35DP(NW),
     &ReF37(NW),UrF37(NW),ReF37DP(NW),UrF37DP(NW),
     &ReG39(NW),UrG39(NW),ReG39DP(NW),UrG39DP(NW),
     &ReD33(NW),UrD33(NW),ReD33DP(NW),UrD33DP(NW),
     &ReF35(NW),UrF35(NW),ReF35DP(NW),UrF35DP(NW),
     &ReG37(NW),UrG37(NW),ReG37DP(NW),UrG37DP(NW),
     &ReH39(NW),UrH39(NW),ReH39DP(NW),UrH39DP(NW),
     &ReP31(NW),UrP31(NW),ReP31DP(NW),UrP31DP(NW)
      CI=DCMPLX(0.D0,1.D0)
      S31=SPL3(NW,xm,ReS31,ReS31DP,w)+CI*SPL3(NW,xm,UrS31,UrS31DP,w)
      P33=SPL3(NW,xm,ReP33,ReP33DP,w)+CI*SPL3(NW,xm,UrP33,UrP33DP,w)
      D35=SPL3(NW,xm,ReD35,ReD35DP,w)+CI*SPL3(NW,xm,UrD35,UrD35DP,w)
      F37=SPL3(NW,xm,ReF37,ReF37DP,w)+CI*SPL3(NW,xm,UrF37,UrF37DP,w)
      G39=SPL3(NW,xm,ReG39,ReG39DP,w)+CI*SPL3(NW,xm,UrG39,UrG39DP,w)
      D33=SPL3(NW,xm,ReD33,ReD33DP,w)+CI*SPL3(NW,xm,UrD33,UrD33DP,w)
      F35=SPL3(NW,xm,ReF35,ReF35DP,w)+CI*SPL3(NW,xm,UrF35,UrF35DP,w)
      G37=SPL3(NW,xm,ReG37,ReG37DP,w)+CI*SPL3(NW,xm,UrG37,UrG37DP,w)
      H39=SPL3(NW,xm,ReH39,ReH39DP,w)+CI*SPL3(NW,xm,UrH39,UrH39DP,w)
      
      pm2=pm**2

      p1=dsqrt(xLbd(w**2,pm**2,ti)/(4.d0*w**2))
      p2=dsqrt(xLbd(w**2,pm**2,pim**2)/(4.d0*w**2))
      
      costhcm=(t-2.d0*pm2+2.d0*dsqrt(p1**2+pm2)*dsqrt(p2**2+pm2))
     &/(2.d0*p1*p2)
     
!       print*,"f1_32 pm2=",pm2,"costh=",costhcm
      
       f1_32=1.0d0/p2*(S31*LegendrePrim(1,costhcm)+
     & P33*LegendrePrim(2,costhcm)+D35*LegendrePrim(3,costhcm)+
     & F37*LegendrePrim(4,costhcm)+G39*LegendrePrim(5,costhcm)-
     & D33*LegendrePrim(1,costhcm)-F35*LegendrePrim(2,costhcm)-
     & G37*LegendrePrim(3,costhcm)-H39*LegendrePrim(4,costhcm)
     & )
       end

      complex*16 function f2_32(w,t,ti)
      implicit none
      integer NW
      real*8 LegendrePrim,xm,pm2,p1,p2,xLbd,costhcm,w,t,ti,SPL3
      real*8 pm,pim,rom,omegm
      real*8 pi,e,gpipiro,GrhoV,GrhoT,GomV,GomT,groompi,gropigam,
     &gompigam,CONV
      real*8 ReS31,UrS31,ReS31DP,UrS31DP,
     &ReP33,UrP33,ReP33DP,UrP33DP,
     &ReD35,UrD35,ReD35DP,UrD35DP,
     &ReF37,UrF37,ReF37DP,UrF37DP,
     &ReG39,UrG39,ReG39DP,UrG39DP,
     &ReD33,UrD33,ReD33DP,UrD33DP,
     &ReF35,UrF35,ReF35DP,UrF35DP,
     &ReG37,UrG37,ReG37DP,UrG37DP,
     &ReH39,UrH39,ReH39DP,UrH39DP,
     &ReP31,UrP31,ReP31DP,UrP31DP
      complex*16 CI,P31,P33,D35,F37,G39,D33,F35,G37
      common/masses/pm,pim,rom,omegm
      common/stale/pi,e,gpipiro,GrhoV,GrhoT,GomV,GomT,groompi,gropigam,
     &gompigam,CONV
      parameter(NW=27)
      common/WEZLYPIP/xm(NW)
      common/ampl_pip32/
! I=3/2
     &ReS31(NW),UrS31(NW),ReS31DP(NW),UrS31DP(NW),
     &ReP33(NW),UrP33(NW),ReP33DP(NW),UrP33DP(NW),
     &ReD35(NW),UrD35(NW),ReD35DP(NW),UrD35DP(NW),
     &ReF37(NW),UrF37(NW),ReF37DP(NW),UrF37DP(NW),
     &ReG39(NW),UrG39(NW),ReG39DP(NW),UrG39DP(NW),
     &ReD33(NW),UrD33(NW),ReD33DP(NW),UrD33DP(NW),
     &ReF35(NW),UrF35(NW),ReF35DP(NW),UrF35DP(NW),
     &ReG37(NW),UrG37(NW),ReG37DP(NW),UrG37DP(NW),
     &ReH39(NW),UrH39(NW),ReH39DP(NW),UrH39DP(NW),
     &ReP31(NW),UrP31(NW),ReP31DP(NW),UrP31DP(NW)
      CI=DCMPLX(0.D0,1.D0)
      P31=SPL3(NW,xm,ReP31,ReP31DP,w)+CI*SPL3(NW,xm,UrP31,UrP31DP,w)
      P33=SPL3(NW,xm,ReP33,ReP33DP,w)+CI*SPL3(NW,xm,UrP33,UrP33DP,w)
      D33=SPL3(NW,xm,ReD33,ReD33DP,w)+CI*SPL3(NW,xm,UrD33,UrD33DP,w)
      D35=SPL3(NW,xm,ReD35,ReD35DP,w)+CI*SPL3(NW,xm,UrD35,UrD35DP,w)
      F35=SPL3(NW,xm,ReF35,ReF35DP,w)+CI*SPL3(NW,xm,UrF35,UrF35DP,w)
      F37=SPL3(NW,xm,ReF37,ReF37DP,w)+CI*SPL3(NW,xm,UrF37,UrF37DP,w)
      G37=SPL3(NW,xm,ReG37,ReG37DP,w)+CI*SPL3(NW,xm,UrG37,UrG37DP,w)
      G39=SPL3(NW,xm,ReG39,ReG39DP,w)+CI*SPL3(NW,xm,UrG39,UrG39DP,w) 
      
      pm2=pm**2

      p1=dsqrt(xLbd(w**2,pm**2,ti)/(4.d0*w**2))
      p2=dsqrt(xLbd(w**2,pm**2,pim**2)/(4.d0*w**2))
      
      costhcm=(t-2.d0*pm2+2.d0*dsqrt(p1**2+pm2)*dsqrt(p2**2+pm2))
     &/(2.d0*p1*p2)
     
!       print*,"f2_32 pm2=",pm2,"costh=",costhcm
     
       f2_32=1.d0/p2*((P31-P33)*LegendrePrim(1,costhcm)+
     & (D33-D35)*LegendrePrim(2,costhcm)+
     & (F35-F37)*LegendrePrim(3,costhcm)+
     & (G37-G39)*LegendrePrim(4,costhcm)
     & )
       end
! funkcje f1 i f2 wchodzące do amplitudy rozpraszania pi-p
