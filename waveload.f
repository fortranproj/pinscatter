      subroutine load_pip_waves()
      implicit real*8 (a-h,o-z)
      integer GetAmpl
      parameter(NW=27)
      common/WEZLYPIP/xmasy(NW)
      common/ampl_pip12/
! I=1/2
     &ReS11(NW),UrS11(NW),ReS11DP(NW),UrS11DP(NW),
     &ReP13(NW),UrP13(NW),ReP13DP(NW),UrP13DP(NW),
     &ReD15(NW),UrD15(NW),ReD15DP(NW),UrD15DP(NW),
     &ReF17(NW),UrF17(NW),ReF17DP(NW),UrF17DP(NW),
     &ReG19(NW),UrG19(NW),ReG19DP(NW),UrG19DP(NW),
     &ReD13(NW),UrD13(NW),ReD13DP(NW),UrD13DP(NW),
     &ReF15(NW),UrF15(NW),ReF15DP(NW),UrF15DP(NW),
     &ReG17(NW),UrG17(NW),ReG17DP(NW),UrG17DP(NW),
     &ReH19(NW),UrH19(NW),ReH19DP(NW),UrH19DP(NW),
     &ReP11(NW),UrP11(NW),ReP11DP(NW),UrP11DP(NW)
      common/ampl_pip32/
! I=3/2
     &ReS31(NW),UrS31(NW),ReS31DP(NW),UrS31DP(NW),
     &ReP33(NW),UrP33(NW),ReP33DP(NW),UrP33DP(NW),
     &ReD35(NW),UrD35(NW),ReD35DP(NW),UrD35DP(NW),
     &ReF37(NW),UrF37(NW),ReF37DP(NW),UrF37DP(NW),
     &ReG39(NW),UrG39(NW),ReG39DP(NW),UrG39DP(NW),
     &ReD33(NW),UrD33(NW),ReD33DP(NW),UrD33DP(NW),
     &ReF35(NW),UrF35(NW),ReF35DP(NW),UrF35DP(NW),
     &ReG37(NW),UrG37(NW),ReG37DP(NW),UrG37DP(NW),
     &ReH39(NW),UrH39(NW),ReH39DP(NW),UrH39DP(NW),
     &ReP31(NW),UrP31(NW),ReP31DP(NW),UrP31DP(NW)
     
! te cztery tablice są tylko paramterami funkcji liczącej drugie pochodne 
! do interpolacji amplitudy pi-p. Nie są nigdzie wykorzystywane
      double precision Wre(NW),Bre(NW),Ure(NW),Vre(NW)

! import amplitud czastkowych pi-p
! UWAGA: W plikach masa jest wyrażona w MeVach - przy imporcie do tablic następuje konwersja na GeVy (w funkcji GetAmpl)
      print*,'Amplitude import, I=1/2...'
      if(GetAmpl('S11.dat',NW,xmasy,ReS11,UrS11).GT.0) then
      print*,'Error while reading S11'
      stop
      endif
      if(GetAmpl('P13.dat',NW,xmasy,ReP13,UrP13).GT.0) then
      print*,'Error while reading P13'
      stop
      endif
      if(GetAmpl('D15.dat',NW,xmasy,ReD15,UrD15).GT.0) then
      print*,'Error while reading D15'
      stop
      endif
      if(GetAmpl('F17.dat',NW,xmasy,ReF17,UrF17).GT.0) then
      print*,'Error while reading F17'
      stop
      endif
      if(GetAmpl('G19.dat',NW,xmasy,ReG19,UrG19).GT.0) then
      print*,'Error while reading G19'
      stop
      endif
      if(GetAmpl('D13.dat',NW,xmasy,ReD13,UrD13).GT.0) then
      print*,'Error while reading D13'
      stop
      endif
      if(GetAmpl('F15.dat',NW,xmasy,ReF15,UrF15).GT.0) then
      print*,'Error while reading F15'
      stop
      endif
      if(GetAmpl('G17.dat',NW,xmasy,ReG17,UrG17).GT.0) then
      print*,'Error while reading G17'
      stop
      endif
      if(GetAmpl('H19.dat',NW,xmasy,ReH19,UrH19).GT.0) then
      print*,'Error while reading H19'
      stop
      endif
      if(GetAmpl('P11.dat',NW,xmasy,ReP11,UrP11).GT.0) then
      print*,'Error while reading P11'
      stop
      endif
      print*,'OK'

      print*,'Calculating second derivatives, I=1/2...'
      CALL ZSPL3(NW,xmasy,ReS11,Wre,Bre,Ure,Vre,ReS11DP)
      CALL ZSPL3(NW,xmasy,UrS11,Wre,Bre,Ure,Vre,UrS11DP)
      CALL ZSPL3(NW,xmasy,ReP13,Wre,Bre,Ure,Vre,ReP13DP)
      CALL ZSPL3(NW,xmasy,UrP13,Wre,Bre,Ure,Vre,UrP13DP)
      CALL ZSPL3(NW,xmasy,ReD15,Wre,Bre,Ure,Vre,ReD15DP)
      CALL ZSPL3(NW,xmasy,UrD15,Wre,Bre,Ure,Vre,UrD15DP)
      CALL ZSPL3(NW,xmasy,ReF17,Wre,Bre,Ure,Vre,ReF17DP)
      CALL ZSPL3(NW,xmasy,UrF17,Wre,Bre,Ure,Vre,UrF17DP)
      CALL ZSPL3(NW,xmasy,ReG19,Wre,Bre,Ure,Vre,ReG19DP)
      CALL ZSPL3(NW,xmasy,UrG19,Wre,Bre,Ure,Vre,UrG19DP)
      CALL ZSPL3(NW,xmasy,ReD13,Wre,Bre,Ure,Vre,ReD13DP)
      CALL ZSPL3(NW,xmasy,UrD13,Wre,Bre,Ure,Vre,UrD13DP)
      CALL ZSPL3(NW,xmasy,ReF15,Wre,Bre,Ure,Vre,ReF15DP)
      CALL ZSPL3(NW,xmasy,UrF15,Wre,Bre,Ure,Vre,UrF15DP)
      CALL ZSPL3(NW,xmasy,ReG17,Wre,Bre,Ure,Vre,ReG17DP)
      CALL ZSPL3(NW,xmasy,UrG17,Wre,Bre,Ure,Vre,UrG17DP)
      CALL ZSPL3(NW,xmasy,ReH19,Wre,Bre,Ure,Vre,ReH19DP)
      CALL ZSPL3(NW,xmasy,UrH19,Wre,Bre,Ure,Vre,UrH19DP)
      CALL ZSPL3(NW,xmasy,ReP11,Wre,Bre,Ure,Vre,ReP11DP)
      CALL ZSPL3(NW,xmasy,UrP11,Wre,Bre,Ure,Vre,UrP11DP)
      print*,'OK'

      print*,'Amplitude import, I=3/2...'
      if(GetAmpl('S31.dat',NW,xmasy,ReS31,UrS31).GT.0) then
      print*,'Error while reading S31'
      stop
      endif
      if(GetAmpl('P33.dat',NW,xmasy,ReP33,UrP33).GT.0) then
      print*,'Error while reading P33'
      stop
      endif
      if(GetAmpl('D35.dat',NW,xmasy,ReD35,UrD35).GT.0) then
      print*,'Error while reading D35'
      stop
      endif
      if(GetAmpl('F37.dat',NW,xmasy,ReF37,UrF37).GT.0) then
      print*,'Error while reading F37'
      stop
      endif
      if(GetAmpl('G39.dat',NW,xmasy,ReG39,UrG39).GT.0) then
      print*,'Error while reading G39'
      stop
      endif
      if(GetAmpl('D33.dat',NW,xmasy,ReD33,UrD33).GT.0) then
      print*,'Error while reading D33'
      stop
      endif
      if(GetAmpl('F35.dat',NW,xmasy,ReF35,UrF35).GT.0) then
      print*,'Error while reading F35'
      stop
      endif
      if(GetAmpl('G37.dat',NW,xmasy,ReG37,UrG37).GT.0) then
      print*,'Error while reading G37'
      stop
      endif
      if(GetAmpl('H39.dat',NW,xmasy,ReH39,UrH39).GT.0) then
      print*,'Error while reading H39'
      stop
      endif
      if(GetAmpl('P31.dat',NW,xmasy,ReP31,UrP31).GT.0) then
      print*,'Error while reading P31'
      stop
      endif
      print*,'OK'

      print*,'Calculating second derivatives, I=3/2...'
      CALL ZSPL3(NW,xmasy,ReS31,Wre,Bre,Ure,Vre,ReS31DP)
      CALL ZSPL3(NW,xmasy,UrS31,Wre,Bre,Ure,Vre,UrS31DP)
      CALL ZSPL3(NW,xmasy,ReP33,Wre,Bre,Ure,Vre,ReP33DP)
      CALL ZSPL3(NW,xmasy,UrP33,Wre,Bre,Ure,Vre,UrP33DP)
      CALL ZSPL3(NW,xmasy,ReD35,Wre,Bre,Ure,Vre,ReD35DP)
      CALL ZSPL3(NW,xmasy,UrD35,Wre,Bre,Ure,Vre,UrD35DP)
      CALL ZSPL3(NW,xmasy,ReF37,Wre,Bre,Ure,Vre,ReF37DP)
      CALL ZSPL3(NW,xmasy,UrF37,Wre,Bre,Ure,Vre,UrF37DP)
      CALL ZSPL3(NW,xmasy,ReG39,Wre,Bre,Ure,Vre,ReG39DP)
      CALL ZSPL3(NW,xmasy,UrG39,Wre,Bre,Ure,Vre,UrG39DP)
      CALL ZSPL3(NW,xmasy,ReD33,Wre,Bre,Ure,Vre,ReD33DP)
      CALL ZSPL3(NW,xmasy,UrD33,Wre,Bre,Ure,Vre,UrD33DP)
      CALL ZSPL3(NW,xmasy,ReF35,Wre,Bre,Ure,Vre,ReF35DP)
      CALL ZSPL3(NW,xmasy,UrF35,Wre,Bre,Ure,Vre,UrF35DP)
      CALL ZSPL3(NW,xmasy,ReG37,Wre,Bre,Ure,Vre,ReG37DP)
      CALL ZSPL3(NW,xmasy,UrG37,Wre,Bre,Ure,Vre,UrG37DP)
      CALL ZSPL3(NW,xmasy,ReH39,Wre,Bre,Ure,Vre,ReH39DP)
      CALL ZSPL3(NW,xmasy,UrH39,Wre,Bre,Ure,Vre,UrH39DP)
      CALL ZSPL3(NW,xmasy,ReP31,Wre,Bre,Ure,Vre,ReP31DP)
      CALL ZSPL3(NW,xmasy,UrP31,Wre,Bre,Ure,Vre,UrP31DP)
      print*,'OK'
      end

      
! function to import pi-p partial wave amplitudes
! if return code <=0 import OK, if return code >=0 error
       integer function GetAmpl(filename,filerows,a_mass,a_re,a_im)
       implicit double precision(a-h,o-z)
       character*7 filename
       character*45 naglowek
       character*12 path
       integer filerows,statuspliku
       character*19 filepath
       dimension a_mass(filerows),a_re(filerows),a_im(filerows)
! headrows - number of header lines
       parameter (iheadrows=2,path='ampl_czastk/')

       filepath=path//filename
       open(81,file=filepath,status='old'
     & ,iostat=statuspliku)
       do i=1,iheadrows
       read(81,*)naglowek
       enddo
       do i=1,filerows
       read(81,*)a_mass(i),del,delerr,sr,err,a_re(i),
     & a_im(i),p1,p2
! MeV to GeV conversion
       a_mass(i)=a_mass(i)/1000.d0
       enddo

       GetAmpl=statuspliku
       return
       end
