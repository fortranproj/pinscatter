      program main
      implicit none
      complex*16 apip_roex,app,apm,amp,amm,Tminus,
     &Aminus,Bminus,a,b
      real*8 ::W,del,costh,xLbd,totalcspiplp,s
      integer :: i,j,k,imax
      real*8 pm,pim,rom,omegm
      real*8 sqsaid,sqro
      common/masses/pm,pim,rom,omegm
      
      real*8:: pmod=1.d0
      real*8:: ct2
      real*8:: phi=dacos(-1.d0)/2.d0
      integer:: l=1
      complex*16 upl(4),ubarpl(4),umi(4),ubarmi(4),u1baru2
      
      call initCommons()
      
!       costh=.2d0
!       ct2=dsqrt((1.d0+costh)/2.d0)
!       call bispinor(upl,pmod,ct2,phi,1)
!       call dirconj(upl,ubarpl)
!       call bispinor(umi,pmod,ct2,phi,-1)
!       call dirconj(umi,ubarmi)
! 
!       write(*,45)upl
!       write(*,45)ubarpl
! 
!       write(*,45)umi
!       write(*,45)ubarmi
!       
!       print*,u1baru2(ubarpl,upl)
!       print*,u1baru2(ubarmi,upl)
! 45    format(8F5.2)      
!       
!       stop
      
      
!       open(unit=1,file="appi-roex.dat",status="unknown")
!       open(unit=2,file="Tppi-roex-W010.dat",status="unknown")
!       open(unit=3,file="Tppi-said-W010.dat",status="unknown")
!       open(unit=4,file="AB-said-W.dat",status="unknown")
!       open(unit=5,file="Asq-W.dat",status="unknown")
      open(unit=7,file="totalcspiplp.dat",status="unknown")
      
      
      
      call load_pip_waves()

!       write(1,1)"cos","Re++","Im++","Re+-","Im+-","Re-+","Im-+",
!      &"Re--","Im--"
! 1     format(A5,8A10)
!         W=1.5d0
!         del=0.05d0
! 
!       imax=2.d0/del
!       do i=0,imax
!       costh=-1.d0+i*del
!       app=apip_roex(W**2,costh,1,1)
!       apm=apip_roex(W**2,costh,1,-1)
!       amp=apip_roex(W**2,costh,-1,1)
!       amm=apip_roex(W**2,costh,-1,-1)
!       write(1,2)costh,dble(app),dimag(app),dble(apm),dimag(apm),
!      &dble(amp),dimag(amp),dble(amm),dimag(amm)
! 2     format(F5.2,8F10.3)
!       print*,costh
!       enddo
! 
! 
!       costh=0.99999999999999d0
!       write(2,*)"#costh=",costh
!       write(2,3)"W","Re++","Im++","Re+-","Im+-","Re-+","Im-+",
!      &"Re--","Im--"
! 3     format(A5,8A10)
!       del=0.05d0
! 
!       imax=(2.7-pm-pim)/del
!       do i=0,imax
!       W=pm+pim+1.d-4+i*del
!       app=apip_roex(W**2,costh,1,1)
!       apm=apip_roex(W**2,costh,1,-1)
!       amp=apip_roex(W**2,costh,-1,1)
!       amm=apip_roex(W**2,costh,-1,-1)
!       write(2,4)W,dble(app),dimag(app),dble(apm),dimag(apm),
!      &dble(amp),dimag(amp),dble(amm),dimag(amm)
! 4     format(F5.2,8F10.3)
!       print*,W,dsqrt(xLbd(W**2,pm**2,pim**2)/(4.d0*W**2))
!       enddo
!       
!       print*,"rho-exchange finished"
!       
!       write(3,*)"#costh=",costh
!       write(3,5)"#W","Re++","Im++","Re+-","Im+-","Re-+","Im-+",
!      &"Re--","Im--"
! 5     format(A5,8A10)
!       del=0.05d0
! 
!       imax=(2.7-pm-pim)/del
!       do i=0,imax
!       W=pm+pim+1.d-4+i*del
!       app=Tminus(W**2,costh,1,1)
!       apm=Tminus(W**2,costh,1,-1)
!       amp=Tminus(W**2,costh,-1,1)
!       amm=Tminus(W**2,costh,-1,-1)
!       write(3,6)W,dble(app),dimag(app),dble(apm),dimag(apm),
!      &dble(amp),dimag(amp),dble(amm),dimag(amm)
! 6     format(F5.2,8F10.3)
!       print*,W,dsqrt(xLbd(W**2,pm**2,pim**2)/(4.d0*W**2))
!       enddo
!       
!       print*,"SAID finished"
      

!       write(4,7)"W","ReA","ImA","ReB","ImB"
! 7     format(A5,4A10)
!       del=0.01d0
!       print*,"Threshold: ",pm+pim,"GeV."
!       imax=(2.7-pm-pim)/del
!       do i=0,imax
!       W=pm+pim+1.d-4+i*del
!       costh=0.9d0
!       a=Aminus(W**2,-0.0000000001d0)
!       b=Bminus(W**2,-0.0000000001d0)
!       
!       write(4,8)W,dble(a),dimag(a),dble(b),dimag(b)
! 8     format(F5.2,4F10.3)
!       print*,W
!       enddo

!       write(5,9)"#W","Aro^2","Asaid^2"
! 9     format(A5,2A10)
!       del=0.01d0
! 
!       imax=(2.7-pm-pim)/del
!       do i=0,imax
!       W=pm+pim+1.d-4+i*del
!       costh=0.9d0
!       sqsaid=0.d0
!       sqro=0.d0
!       do j=-1,1,2
!       do k=-1,1,2
!       sqsaid=sqsaid+cdabs(Tminus(W**2,costh,j,k))**2
!       sqro=sqro+cdabs(apip_roex(W**2,costh,j,k))**2
!       enddo
!       enddo
!       write(5,10)W,sqro,sqsaid
! 10    format(F5.2,2F10.1)
!       print*,W
!       enddo

      write(7,11)"#W","totalcs"
11    format(A5,A10)
      del=0.01d0

      imax=(2.7-pm-pim)/del
      do i=0,imax
      W=pm+pim+1.d-4+i*del

      write(7,12)W,totalcspiplp(W**2,0.d0)
12    format(F5.2,F10.3)
      print*,W
      enddo
      
      
      end
