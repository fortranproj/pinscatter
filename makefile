#compiler
FC=gfortran
OBJ=main.o waveload.o apip_roex.o apip_said.o\
../lblibs/init_amps.o ../lblibs/dirac_utils.o ../lblibs/regge.o ../lblibs/numerics.o ../lblibs/gauss_int.o ../lblibs/kinem_utils.o
FFLAGS= -std=legacy -O2
main: $(OBJ)
	$(FC) $(FFLAGS) -o $@ $(OBJ)
clean:
	rm main *.o ../lblibs/*.o
